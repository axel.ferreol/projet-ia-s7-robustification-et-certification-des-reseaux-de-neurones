import argparse
import time
import numpy as np
import torch.nn as nn
import torch
import os
import utils
from datetime import datetime
np.set_printoptions(precision=5, suppress=True)


def p_selection(p_init, it, n_iters):
    """ Piece-wise constant schedule for p (the fraction of pixels changed on every iteration). """
    it = int(it / n_iters * 10000)

    if 10 < it <= 50:
        p = p_init / 2
    elif 50 < it <= 200:
        p = p_init / 4
    elif 200 < it <= 500:
        p = p_init / 8
    elif 500 < it <= 1000:
        p = p_init / 16
    elif 1000 < it <= 2000:
        p = p_init / 32
    elif 2000 < it <= 4000:
        p = p_init / 64
    elif 4000 < it <= 6000:
        p = p_init / 128
    elif 6000 < it <= 8000:
        p = p_init / 256
    elif 8000 < it <= 10000:
        p = p_init / 512
    else:
        p = p_init

    return p


def pseudo_gaussian_pert_rectangles(x, y):
    delta = np.zeros([x, y])
    x_c, y_c = x // 2 + 1, y // 2 + 1

    counter2 = [x_c - 1, y_c - 1]
    for counter in range(0, max(x_c, y_c)):
        delta[max(counter2[0], 0):min(counter2[0] + (2 * counter + 1), x),
              max(0, counter2[1]):min(counter2[1] + (2 * counter + 1), y)] += 1.0 / (counter + 1) ** 2

        counter2[0] -= 1
        counter2[1] -= 1

    delta /= np.sqrt(np.sum(delta ** 2, keepdims=True))

    return delta


def meta_pseudo_gaussian_pert(s):
    delta = np.zeros([s, s])
    n_subsquares = 2
    if n_subsquares == 2:
        delta[:s // 2] = pseudo_gaussian_pert_rectangles(s // 2, s)
        delta[s // 2:] = pseudo_gaussian_pert_rectangles(s - s // 2, s) * (-1)
        delta /= np.sqrt(np.sum(delta ** 2, keepdims=True))
        if np.random.rand(1) > 0.5:
            delta = np.transpose(delta)

    elif n_subsquares == 4:
        delta[:s // 2, :s // 2] = pseudo_gaussian_pert_rectangles(
            s // 2, s // 2) * np.random.choice([-1, 1])
        delta[s // 2:, :s // 2] = pseudo_gaussian_pert_rectangles(
            s - s // 2, s // 2) * np.random.choice([-1, 1])
        delta[:s // 2, s // 2:] = pseudo_gaussian_pert_rectangles(
            s // 2, s - s // 2) * np.random.choice([-1, 1])
        delta[s // 2:, s // 2:] = pseudo_gaussian_pert_rectangles(
            s - s // 2, s - s // 2) * np.random.choice([-1, 1])
        delta /= np.sqrt(np.sum(delta ** 2, keepdims=True))

    return delta


def softmax(x):
    e_x = np.exp(x - np.max(x, axis=1, keepdims=True))
    return e_x / e_x.sum(axis=1, keepdims=True)


def loss_f(label, res, loss_type="margin_loss"):
    """ Implements the margin loss (difference between the correct and 2nd best class). """
    if loss_type == "margin_loss":
        a = res-res[np.arange(label.shape[0]), label][:, None]
        a[np.arange(label.shape[0]), label] = np.inf
        return (-1*torch.min(a, dim=1)[0]).detach().numpy()
    else:
        return nn.CrossEntropyLoss(reduce=False)(res, label).detach().numpy()


def square_attack_l2(net, data, label, eps, n_iters, p_init, loss_type):
    model = net
    x = data.detach().numpy()
    y = label.detach().numpy()

    """ The L2 square attack """
    np.random.seed(0)

    min_val, max_val = -1, 1
    c, h, w = x.shape[1:]
    n_features = c * h * w

    # initialization
    delta_init = np.zeros(x.shape)
    s = h // 5
    sp_init = (h - s * 5) // 2
    center_h = sp_init + 0
    for counter in range(h // s):
        center_w = sp_init + 0
        for counter2 in range(w // s):
            delta_init[:, :, center_h:center_h + s, center_w:center_w + s] += meta_pseudo_gaussian_pert(s).reshape(
                [1, 1, s, s]) * np.random.choice([-1, 1], size=[x.shape[0], c, 1, 1])
            center_w += s
        center_h += s
    delta_init[0].squeeze().shape

    x_best = np.clip(x + delta_init / np.sqrt(np.sum(delta_init **
                                                     2, axis=(1, 2, 3), keepdims=True)) * eps, 0, 1)

    logits = model(torch.tensor(x_best, dtype=torch.float32))
    loss_min = loss_f(torch.tensor(y), logits, loss_type=loss_type)
    margin_min = loss_f(torch.tensor(y), logits, loss_type='margin_loss')

    # ones because we have already used 1 query
    n_queries = np.ones(x.shape[0])

    for i_iter in range(n_iters):
        idx_to_fool = (margin_min > 0.0)

        x_curr, x_best_curr = x[idx_to_fool], x_best[idx_to_fool]
        y_curr, margin_min_curr = y[idx_to_fool], margin_min[idx_to_fool]
        loss_min_curr = loss_min[idx_to_fool]
        delta_curr = x_best_curr - x_curr

        p = p_selection(p_init, i_iter, n_iters)
        s = max(int(round(np.sqrt(p * n_features / c))), 3)

        if s % 2 == 0:
            s += 1

        s2 = s + 0
        # window_1
        center_h = np.random.randint(0, h - s)
        center_w = np.random.randint(0, w - s)
        new_deltas_mask = np.zeros(x_curr.shape)
        new_deltas_mask[:, :, center_h:center_h +
                        s, center_w:center_w + s] = 1.0

        # window_2
        center_h_2 = np.random.randint(0, h - s2)
        center_w_2 = np.random.randint(0, w - s2)
        new_deltas_mask_2 = np.zeros(x_curr.shape)
        new_deltas_mask_2[:, :, center_h_2:center_h_2 +
                          s2, center_w_2:center_w_2 + s2] = 1.0

        # compute total norm available
        curr_norms_window = np.sqrt(
            np.sum(((x_best_curr - x_curr) * new_deltas_mask) ** 2, axis=(2, 3), keepdims=True))
        curr_norms_image = np.sqrt(
            np.sum((x_best_curr - x_curr) ** 2, axis=(1, 2, 3), keepdims=True))
        mask_2 = np.maximum(new_deltas_mask, new_deltas_mask_2)
        norms_windows = np.sqrt(
            np.sum((delta_curr * mask_2) ** 2, axis=(2, 3), keepdims=True))

        # create the updates
        new_deltas = np.ones([x_curr.shape[0], c, s, s])
        new_deltas = new_deltas * \
            meta_pseudo_gaussian_pert(s).reshape([1, 1, s, s])
        new_deltas *= np.random.choice([-1, 1],
                                       size=[x_curr.shape[0], c, 1, 1])
        old_deltas = delta_curr[:, :, center_h:center_h + s,
                                center_w:center_w + s] / (1e-10 + curr_norms_window)
        new_deltas += old_deltas
        new_deltas = new_deltas / np.sqrt(np.sum(new_deltas ** 2, axis=(2, 3), keepdims=True)) * (
            np.maximum(eps ** 2 - curr_norms_image ** 2, 0) / c + norms_windows ** 2) ** 0.5
        delta_curr[:, :, center_h_2:center_h_2 + s2,
                   center_w_2:center_w_2 + s2] = 0.0  # set window_2 to 0
        delta_curr[:, :, center_h:center_h + s,
                   center_w:center_w + s] = new_deltas + 0  # update window_1

        x_new = x_curr + delta_curr / \
            np.sqrt(np.sum(delta_curr ** 2, axis=(1, 2, 3), keepdims=True)) * eps
        x_new = np.clip(x_new, min_val, max_val)
        curr_norms_image = np.sqrt(
            np.sum((x_new - x_curr) ** 2, axis=(1, 2, 3), keepdims=True))

        logits = model(torch.tensor(x_new, dtype=torch.float32))
        loss = loss_f(torch.tensor(y_curr), logits, loss_type=loss_type)
        margin = loss_f(torch.tensor(y_curr), logits, loss_type='margin_loss')

        idx_improved = loss < loss_min_curr
        loss_min[idx_to_fool] = idx_improved * \
            loss + ~idx_improved * loss_min_curr
        margin_min[idx_to_fool] = idx_improved * \
            margin + ~idx_improved * margin_min_curr

        idx_improved = np.reshape(idx_improved, [-1, *[1] * len(x.shape[:-1])])
        x_best[idx_to_fool] = idx_improved * \
            x_new + ~idx_improved * x_best_curr
        n_queries[idx_to_fool] += 1
    return n_queries, torch.tensor(x_best, dtype=torch.float32)


def square_attack_linf(model, data, label, eps, n_iters, p_init, loss_type):
    x = data.detach().numpy()
    y = label.detach().numpy()
    """ The Linf square attack """
    np.random.seed(0)  # important to leave it here as well
    min_val, max_val = -1, 1
    c, h, w = x.shape[1:]
    n_features = c*h*w

    # [c, 1, w], i.e. vertical stripes work best for untargeted attacks
    init_delta = np.random.choice([-eps, eps], size=[x.shape[0], c, 1, w])
    x_best = np.clip(x + init_delta, min_val, max_val)

    logits = model(torch.tensor(x_best, dtype=torch.float32))
    loss_min = loss_f(label, logits,  loss_type=loss_type)
    margin_min = loss_f(label, logits,  loss_type='margin_loss')
    # ones because we have already used 1 query
    n_queries = np.ones(x.shape[0])

    for i_iter in range(n_iters - 1):
        idx_to_fool = margin_min > 0
        x_curr, x_best_curr, y_curr = x[idx_to_fool], x_best[idx_to_fool], y[idx_to_fool]
        loss_min_curr, margin_min_curr = loss_min[idx_to_fool], margin_min[idx_to_fool]
        deltas = x_best_curr - x_curr

        p = p_selection(p_init, i_iter, n_iters)
        for i_img in range(x_best_curr.shape[0]):
            s = int(round(np.sqrt(p * n_features / c)))
            # at least c x 1 x 1 window is taken and at most c x h-1 x h-1
            s = min(max(s, 1), h-1)
            center_h = np.random.randint(0, h - s)
            center_w = np.random.randint(0, w - s)

            x_curr_window = x_curr[i_img, :,
                                   center_h:center_h+s, center_w:center_w+s]
            x_best_curr_window = x_best_curr[i_img, :,
                                             center_h:center_h+s, center_w:center_w+s]
            # prevent trying out a delta if it doesn't change x_curr (e.g. an overlapping patch)
            while np.sum(np.abs(np.clip(x_curr_window + deltas[i_img, :, center_h:center_h+s, center_w:center_w+s], min_val, max_val) - x_best_curr_window) < 10**-7) == c*s*s:
                deltas[i_img, :, center_h:center_h+s, center_w:center_w +
                       s] = np.random.choice([-eps, eps], size=[c, 1, 1])

        x_new = np.clip(x_curr + deltas, min_val, max_val)

        logits = model(torch.tensor(x_new, dtype=torch.float32))

        loss = loss_f(torch.tensor(y_curr), logits,  loss_type=loss_type)
        margin = loss_f(torch.tensor(y_curr), logits,  loss_type='margin_loss')

        idx_improved = loss < loss_min_curr
        loss_min[idx_to_fool] = idx_improved * \
            loss + ~idx_improved * loss_min_curr
        margin_min[idx_to_fool] = idx_improved * \
            margin + ~idx_improved * margin_min_curr
        idx_improved = np.reshape(idx_improved, [-1, *[1]*len(x.shape[:-1])])
        x_best[idx_to_fool] = idx_improved * \
            x_new + ~idx_improved * x_best_curr
        n_queries[idx_to_fool] += 1

    return n_queries, torch.tensor(x_best, dtype=torch.float32)
