import torch
import torch.optim as optim
import torch.nn.functional as F
import torch.nn as nn


def fgsm_attack(model, data, label, epsilon):
    """ Construct FGSM adversarial examples on the examples X"""
    delta = torch.zeros_like(data, requires_grad=True)
    loss = F.nll_loss(model(data + delta), label)
    loss.backward()
    return torch.clamp(data+epsilon * delta.grad.detach().sign(), -1, 1)


def pgd_linf(model, data, label, epsilon, alpha, num_iter):
    """ Construct PGD adversarial examples on the samples X"""
    delta = torch.zeros_like(data, requires_grad=True)
    for t in range(num_iter):
        loss = F.nll_loss(model((data + delta).clamp(-1, 1)), label)
        loss.backward()
        delta.data = (delta + alpha*delta.grad.detach().sign()
                      ).clamp(-epsilon, epsilon)
        delta.grad.zero_()
    return (data+delta).clamp(-1, 1).detach()


def pgd_linf_rand(model, data, label, epsilon, alpha, num_iter, restarts):
    """ Construct PGD adversarial examples on the samples X, with random restarts"""
    max_loss = torch.zeros(label.shape[0])
    max_delta = torch.zeros_like(data)

    for i in range(restarts):
        delta = torch.rand_like(data, requires_grad=True)
        delta.data = delta.data * 2 * epsilon - epsilon

        for t in range(num_iter):
            loss = F.nll_loss(model((data + delta).clamp(-1, 1)), label)
            loss.backward()
            delta.data = (delta + alpha*delta.grad.detach().sign()
                          ).clamp(-epsilon, epsilon)
            delta.grad.zero_()

        all_loss = nn.CrossEntropyLoss(
            reduction='none')(model(data+delta), label)
        max_delta[all_loss >= max_loss] = delta.detach()[all_loss >= max_loss]
        max_loss = torch.max(max_loss, all_loss)

    return (data + max_delta).clamp(-1, 1)
