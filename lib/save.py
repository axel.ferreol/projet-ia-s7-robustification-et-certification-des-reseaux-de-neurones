if __name__ == "__main__":
    import sys
    sys.path.insert(
        1, 'c:/Users/axfer/Documents/Axel_Ferreol/CS_2A/IA/projet-ia-s7-robustification-et-certification-des-reseaux/')

import pandas as pd
from numpy import asarray
from PIL import Image
import torch
import torchvision
from torchvision import transforms
import lib.nnCIRFA10 as nnCIRFA10
import lib.attacks_PGD as attacks_PGD
import matplotlib.image
import matplotlib.pyplot as plt


def unnormalize(img):
    return img/2+0.5


def show_tensor(data):
    plt.imshow(unnormalize(
        data.squeeze().detach().numpy()).transpose((1, 2, 0)))
    plt.axis('off')
    return plt.show()


def save(data, label, index):

    name = f'attack_{index}.png'
    img_data = unnormalize(
        data.squeeze().detach().numpy()).transpose((1, 2, 0))
    # Image.fromarray((img_data*255).astype("uint8")
    #                 ).save(f'./Image_attack/{name}')*
    # nparray[h,w,3]

    matplotlib.image.imsave(f'./CIRFA10_test_attack/{name}', img_data)
    # /!\ nparray[h,w,3+"256"]

    img_labels = pd.read_csv(
        './CIRFA10_test_attack/legende.csv', names=["name", "label"])
    img_labels = img_labels.append(
        {"name": name, "label": label.item()}, ignore_index=True)
    img_labels.to_csv('./CIRFA10_test_attack/legende.csv',
                      index=False, header=False)
    return


def load():
    with Image.open('./Image_attack\\attack_1.png') as img:
        numpydata = asarray(img)
    return numpydata


def generation_pgd_attack():
    # Chargement de réseau de neurones
    net = nnCIRFA10.Net()
    pretrained_model = "./cifar_net.pth"
    net.load_state_dict(torch.load(pretrained_model))

    # Chargement des données
    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    batch_size = 1  # /!\ batch_size = 1

    # trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
    #                                         download=False, transform=transform)
    # trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
    #                                           shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                           download=False, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
                                             shuffle=False, num_workers=2)

    # classes = ('plane', 'car', 'bird', 'cat',
    #            'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    i = 0
    for data, label in testloader:  # trainloader:
        data_attack = attacks_PGD.pgd_attack(net, data, label, 0.07, 3)
        save(data_attack, label, i)
        if i % 1000 == 999:
            print(f"image {i+1} enregistrée")
        i = i+1
    print("Enregistrement terminé")
    return


if __name__ == "__main__":
    generation_pgd_attack()
