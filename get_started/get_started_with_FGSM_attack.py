import torch
import torchvision
import torch.nn.functional as F
from torchvision import transforms
import matplotlib.pyplot as plt
from lib.nnCIRFA10 import Net
import lib.adversial_attack_class as adversial_attack_class

# Chargement de réseau de neurones
net = Net()
pretrained_model = "./cifar_net.pth"
net.load_state_dict(torch.load(pretrained_model))

# Chargement des données
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

batch_size = 4

trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=False, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=False, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
                                         shuffle=False, num_workers=2)


# # PGD attack
# transform_pgd = transforms.Compose(
#     [transforms.ConvertImageDtype(torch.float32), transforms.Normalize((128, 128, 128), (256, 156, 256))])

# trainset_pgd = adversial_attack_dataset.CustomImageDataset(
#     './Image_attack/legende.csv', './Image_attack', transform=transform_pgd)

# trainloader_pgd = torch.utils.data.DataLoader(trainset_pgd, batch_size=batch_size,
#                                               shuffle=True, num_workers=0)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


def fgsm_attack(image, epsilon, data_grad):
    # Collect the element-wise sign of the data gradient
    sign_data_grad = data_grad.sign()
    # Create the perturbed image by adjusting each pixel of the input image
    perturbed_image = image + epsilon*sign_data_grad
    # Adding clipping to maintain [0,1] range
    perturbed_image = torch.clamp(perturbed_image, -1, 1)
    # Return the perturbed image
    return perturbed_image


def fgsm_attack_grad(image, epsilon, data_grad):
    # Collect the element-wise sign of the data gradient
    sign_data_grad = data_grad.sign()
    # Create the perturbed image by adjusting each pixel of the input image
    return epsilon*sign_data_grad


def test(net, testloader, epsilon):

    # Accuracy counter
    correct = 0
    adv_examples = []

    # Loop over all examples in test set
    for data, label in testloader:

        # Send the data and label to the device
        # data, target = data.to(device), target.to(device)

        # Set requires_grad attribute of tensor. Important for Attack
        data.requires_grad = True

        # Forward pass the data through the model
        output = net(data)
        # get the index of the max log-probability
        init_pred = output.max(1, keepdim=True)[1]

        # If the initial prediction is wrong, dont bother attacking, just move on
        if init_pred.item() != label.item():
            continue

        # Calculate the loss
        loss = F.nll_loss(output, label)

        # Zero all existing gradients
        net.zero_grad()

        # Calculate gradients of model in backward pass
        loss.backward()

        # Collect datagrad
        data_grad = data.grad.data

        # Call FGSM Attack
        perturbed_data = fgsm_attack(data, epsilon, data_grad)
        perturbation = fgsm_attack_grad(data, epsilon, data_grad)

        # Re-classify the perturbed image
        output = net(perturbed_data)

        # Check for success
        # get the index of the max log-probability
        final_pred = output.max(1, keepdim=True)[1]
        if final_pred.item() == label.item():
            correct += 1
            # # Special case for saving 0 epsilon examples
            # if (epsilon == 0) and (len(adv_examples) < 5):
            #     adv_ex = perturbed_data.squeeze().detach().cpu().numpy()
            #     adv_examples.append(
            #         (init_pred.item(), final_pred.item(), adv_ex))
        else:
            # Save some adv examples for visualization later
            if len(adv_examples) < 5:
                adv_ex = perturbed_data.squeeze().detach().cpu().numpy()
                perturbation_ex = perturbation.squeeze().detach().cpu().numpy()
                natural_ex = data.squeeze().detach().cpu().numpy()
                adv_examples.append(
                    (init_pred.item(), final_pred.item(), natural_ex, adv_ex, perturbation_ex))

    # Calculate final accuracy for this epsilon
    final_acc = correct/float(len(testloader))
    print("Epsilon: {}\tTest Accuracy = {} / {} = {}".format(epsilon,
                                                             correct, len(testloader), final_acc))

    # Return the accuracy and an adversarial example
    return final_acc, adv_examples


accuracies = {}
examples = {}
epsilons = [0.07]
# Run test for each epsilon
for eps in epsilons:
    acc, ex = test(net,  testloader, eps)
    accuracies[eps] = acc
    examples[eps] = ex


def unnormalize(img):
    return img/2+0.5


def affichage(examples, eps):
    adv_examples = examples[eps]
    n = len(adv_examples)
    plt.figure()
    plt.suptitle(f'FGSM adversial attack , eps = {eps}')
    for i, tuple in enumerate(adv_examples):
        plt.subplot(3, n, i+1)
        plt.imshow(unnormalize(tuple[2]).transpose((1, 2, 0)))
        plt.title(f'{classes[tuple[0]]}')
        plt.subplot(3, n, n+i+1)
        plt.imshow(unnormalize(tuple[3]).transpose((1, 2, 0)))
        plt.title(f'{classes[tuple[1]]}')
        plt.subplot(3, n, 2*n+i+1)
        plt.imshow(unnormalize(tuple[4]).transpose((1, 2, 0)))

    return plt.show()


if __name__ == "__main__":
    affichage(examples[0.07], 0.07)
# Accuracy(epsilon)
# plt.plot(list(accuracies.keys()), list(accuracies.values()))
# plt.title("Accuracy(epsilon)")
# plt.show()
