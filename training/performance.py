if __name__ == "__main__":
    import sys
    # insert at 1, 0 is the script path (or '' in REPL)
    sys.path.insert(
        1, 'c:/Users/axfer/Documents/Axel_Ferreol/CS_2A/IA/projet-ia-s7-robustification-et-certification-des-reseaux/')

import lib.square_attack as square_attack
import lib.data_CIRFA10 as data_CIRFA10
import torch
import lib.nnCIRFA10_91 as nnCIRFA10_91
<<<<<<< HEAD
=======
import lib.attacks_PGD as attacks_PGD
>>>>>>> test
print("Imports ok")

# net = nnCIRFA10_91.SimpleDLA()
# pretrained_model = "./Training/param.pth"
# net.load_state_dict(torch.load(
#     pretrained_model, map_location=torch.device('cpu')))


# SQUARE ATTACK
<<<<<<< HEAD
# CIRFA10_91 : 18.04% erreur squre atatck l_inf
=======
# CIRFA10_91 : 18.04% erreur squre atatck linf
>>>>>>> test
# CIRFA10_91 : 21.30% erreur squre atatck l2 (1000 itérations)


# Teste la performance sur un réseau de neurones "model" en entrant la batch_siez, eps,n_iter, p et loss_type ="xxx"
def performance_square_attack_linf(batch_size, model, eps, n_iter, p, loss_type):
    _, testloader, _ = data_CIRFA10.load_CIRFA10(batch_size)
    n_attack = 0
    for i, X in enumerate(testloader):
        datas, labels = X
        _, x_square_attack = square_attack.square_attack_linf(
            model, datas, labels, eps, n_iter, p, loss_type)
        n_attack = n_attack + \
            (model(x_square_attack).max(dim=1)[1] != labels).sum().item()
        if (i*batch_size) % 100 == 0:
            print(
                f"{(i+1)*batch_size} ème traité ! - {100*n_attack/((i+1)*batch_size)}")
    return n_attack/(len(testloader)*batch_size)


# performance_square_attack_linf(4, net, 7/255, 20, 0.8, "loss_type")


def performance_square_attack_l2(batch_size, model, eps, n_iter, p, loss_type):
    _, testloader, _ = data_CIRFA10.load_CIRFA10(batch_size)
    n_attack = 0
    for i, X in enumerate(testloader):
        datas, labels = X
        _, x_square_attack = square_attack.square_attack_l2(
            model, datas, labels, eps, n_iter, p, loss_type)
        n_attack = n_attack + \
            (model(x_square_attack).max(dim=1)[1] != labels).sum().item()
        if (i*batch_size) % 100 == 0:
            print(
                f"{(i+1)*batch_size} ème traité ! - {100*n_attack/((i+1)*batch_size)}")
    return n_attack/(len(testloader)*batch_size)


# performance_square_attack_l2(4, net, 7/255, 20, 0.8, "loss_type")

# PGD ATTACK
def performance_fgsm(batch_size, model, eps):
    _, testloader, _ = data_CIRFA10.load_CIRFA10(batch_size)
    n_attack = 0
    for i, X in enumerate(testloader):
        datas, labels = X
        x_square_attack = attacks_PGD.fgsm_attack(model, datas, labels, eps)
        n_attack = n_attack + \
            (model(x_square_attack).max(dim=1)[1] != labels).sum().item()
        if (i*batch_size) % 100 == 0:
            print(
                f"{(i+1)*batch_size} ème traité ! - {100*n_attack/((i+1)*batch_size)}")
    return n_attack/(len(testloader)*batch_size)

# performance_fgsm(4, net, 7./255,7./(255*3),10)


def performance_pgd_linf(batch_size, model, eps, alpha, num_iter):

    _, testloader, _ = data_CIRFA10.load_CIRFA10(batch_size)
    n_attack = 0
    for i, X in enumerate(testloader):
        datas, labels = X
        x_square_attack = attacks_PGD.pgd_linf(
            model, datas, labels, eps, alpha, num_iter)
        n_attack = n_attack + \
            (model(x_square_attack).max(dim=1)[1] != labels).sum().item()
        if (i*batch_size) % 100 == 0:
            print(
                f"{(i+1)*batch_size} ème traité ! - {100*n_attack/((i+1)*batch_size)}")
    return n_attack/(len(testloader)*batch_size)


# performance_pgd_linf(4, net, 7./255, 7./(255*3), 10)

def performance_pgd_linf_rand(batch_size, model, eps, alpha, num_iter):
    _, testloader, _ = data_CIRFA10.load_CIRFA10(batch_size)
    n_attack = 0
    for i, X in enumerate(testloader):
        datas, labels = X
        x_square_attack = attacks_PGD.pgd_linf_rand(
            model, datas, labels, eps, alpha, num_iter, 10)
        n_attack = n_attack + \
            (model(x_square_attack).max(dim=1)[1] != labels).sum().item()
        if (i*batch_size) % 100 == 0:
            print(
                f"{(i+1)*batch_size} ème traité ! - {100*n_attack/((i+1)*batch_size)}")
    return n_attack/(len(testloader)*batch_size)


<<<<<<< HEAD
# net = nnCIRFA10_91.SimpleDLA()
# pretrained_model = "./Training/param.pth"
# net.load_state_dict(torch.load(
#     pretrained_model, map_location=torch.device('cpu')))
# performance_square_attack_linf(4, net, 7/255, 20, 0.8, "loss_type")


def performance_square_attack_l2(batch_size, model, eps, n_iter, p, loss_type):
    _, testloader, _ = data_CIRFA10.load_CIRFA10(batch_size)
    n_attack = 0
    for i, X in enumerate(testloader):
        datas, labels = X
        _, x_square_attack = square_attack.square_attack_l2(
            model, datas, labels, eps, n_iter, p, loss_type)
        n_attack = n_attack + \
            (model(x_square_attack).max(dim=1)[1] != labels).sum().item()
        if (i*batch_size) % 100 == 0 and i > 0:
            print(
                f"{i*batch_size} ème traité ! - {100*n_attack/(i*batch_size)}")
    return n_attack/(len(testloader)*batch_size)


# net = nnCIRFA10_91.SimpleDLA()
# pretrained_model = "./Training/param.pth"
# net.load_state_dict(torch.load(
#     pretrained_model, map_location=torch.device('cpu')))
# performance_square_attack_l2(4, net, 7/255, 20, 0.8, "loss_type")
=======
# performance_pgd_linf_rand(1, net, 7/255, 7/(255*3), 10)
>>>>>>> test
