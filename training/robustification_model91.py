if __name__ == "__main__":
    import sys
    sys.path.insert(
        1, 'c:/Users/axfer/Documents/Axel_Ferreol/CS_2A/IA/projet-ia-s7-robustification-et-certification-des-reseaux/')

import torch.nn as nn
import torch
import torchvision
import torchvision.transforms as transforms
import lib.nnCIRFA10_91 as nnCIRFA10_91
import lib.attacks_PGD as attacks_PGD


print("Imports ok")


learning_rate = 1e-3
batch_size = 64
epochs = 1
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
epsilon = 7./255
alpha = epsilon/6
num_iter = 30

transform_train = transforms.Compose([
    transforms.RandomCrop(32, padding=4),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])

transform_test = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])


trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform_train)


trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
                                          shuffle=True, num_workers=2)


testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform_test)

testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
                                         shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


model = nnCIRFA10_91.SimpleDLA()

model.to(device)

# Initialize the loss function
loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(
    model.parameters(), lr=learning_rate, momentum=0.9, weight_decay=5e-4)
scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=200)

acclst = []


def train_loop(dataloader, model, loss_fn, optimizer, epsilon, alpha, num_iter):
    for batch, (X, y) in enumerate(dataloader):
        inputs, labels = X.to(device), y.to(device)
        inputs_attack = attacks_PGD.pgd_linf(
            model, inputs, labels, epsilon, alpha, num_iter)
        # Compute prediction and loss
        pred = model(inputs_attack)
        loss = loss_fn(pred, labels)

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        print(f"{batch*batch_size}")

    PATH = './nn_training_path/nnCIRFA10_91_robust.pth'
    torch.save(model.state_dict(), PATH)
    return


for t in range(epochs):
    print(f"Epoch {t+1}\n-------------------------------")
    train_loop(trainloader, model, loss_fn,
               optimizer, epsilon, alpha, num_iter)
    scheduler.step()
print("Done!")
