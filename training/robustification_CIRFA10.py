import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
import lib.adversial_attack_class as adversial_attack_class
import lib.nnCIRFA10 as nnCIRFA10
import lib.attacks_PGD as attacks_PGD
from lib.nnCIRFA10 import Net


def epoch_adversarial_training(loader, model, epsilon, alpha, num_iter):
    opt = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
    """Adversarial training/evaluation epoch over the dataset"""

    for i, X in enumerate(loader, 0):
        data, label = X
        data_attack = attacks_PGD.pgd_linf(
            model, data, label, epsilon, alpha, num_iter)
        yp = model(data_attack)
        loss = nn.CrossEntropyLoss()(yp, label)
        if opt:
            opt.zero_grad()
            loss.backward()
            opt.step()
        if i % 2000 == 1999:
            print(f"{i+1} èeme image")

    PATH = './cifar_nt_robust_attackl.pth'
    torch.save(net.state_dict(), PATH)
    return


def epoch_adversarial(loader, model, epsilon, alpha, num_iter):
    opt = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
    """Adversarial training/evaluation epoch over the dataset"""
    total_loss, total_err = 0., 0.

    for i, X in enumerate(loader, 0):
        data, label = X
        data_attack = attacks_PGD.pgd_linf(
            model, data, label, epsilon, alpha, num_iter)
        yp = model(data_attack)
        loss = nn.CrossEntropyLoss()(yp, label)
        if opt:
            opt.zero_grad()
            loss.backward()
            opt.step()
        if i % 2000 == 1999:
            print(f"{i+1} ème image")
        total_err += (yp.max(dim=1)[1] != label).sum().item()
        total_loss += loss.item() * data.shape[0]
    return total_err / len(loader.dataset), total_loss / len(loader.dataset)


if __name__ == "__main__":

    net = Net()

    batch_size = 4

    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                            download=False, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
                                              shuffle=True, num_workers=2)

    testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                           download=False, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
                                             shuffle=False, num_workers=2)

    epsilon = 0.07
    alpha = epsilon/2
    num_iter = 15
    #epoch_adversarial_training(trainloader, net, epsilon, alpha, num_iter)
    print(epoch_adversarial(testloader, net, epsilon, alpha, num_iter))
