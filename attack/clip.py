import torch


def clipx(x,initial_input, clip_min, clip_max , sampling ):
      
    
    for n in range(sampling):
        x[0][n] = torch.clip(x[0][n], initial_input[0]+clip_min, initial_input[0]+clip_max)

    return(x)

