import torch
import matplotlib.pyplot as plt
from lib.nnCIRFA10 import Net
import lib.attacks_PGD as attacks_PGD
import torch.nn.functional as F
import lib.nnCIRFA10_91 as nnCIRFA10_91
import lib.data_CIRFA10 as data_CIRFA10
import lib.save as save


def unnormalize(img):
    return img.copy()/2+0.5


def truncate(number, digits):
    stepper = 10 ** digits
    return int(stepper * number) / stepper


# PGD(epsilon) : images
def visualisation_PGD_epsilon(model, data, label, l_epsilon, num_iter):
    l_data_attack = []
    for epsilon in l_epsilon:
        data_attack = attacks_PGD.pgd_linf(
            model, data, label, epsilon, epsilon, num_iter)
        l_data_attack.append(data_attack)

    for i in range(len(l_data_attack)):
        plt.subplot(1+len(l_data_attack)//5, 5, i+1)
        plt.imshow(unnormalize(
            l_data_attack[i][0].squeeze().detach().cpu().numpy()).transpose((1, 2, 0)))
        plt.title(f"Ɛ={truncate(l_epsilon[i],4)}/{int(l_epsilon[i]*255)}")
        plt.axis('off')
    plt.suptitle(
        f"PGD(l_inf) adversial attack with alpha=epsilon, num_iter={num_iter}")
    return plt.show()


# PGD(num_iter) : graphique
def visualisation_PGD_num_iter(model, data, label, epsilon, alpha, l_num_iter):
    l_data_attack = []
    for num_iter in l_num_iter:
        data_attack = attacks_PGD.pgd_linf(
            model, data, label, epsilon, alpha, num_iter)
        l_data_attack.append(F.nll_loss(
            model(data_attack), label).item())

    plt.plot(l_num_iter, l_data_attack)
    plt.xlabel("Number of iterations")
    plt.ylabel("Null loss")
    plt.suptitle(
        f"PGD(l_inf) adversial attack with Ɛ={truncate(epsilon,4)}, α={truncate(alpha,4)}")
    return plt.show()


# PGD(alpha) : graphique
def visualisation_PGD_alpha(model, data, label, epsilon, l_alpha, num_iter):
    l_data_attack = []
    for alpha in l_alpha:
        data_attack = attacks_PGD.pgd_linf(
            model, data, label, epsilon, alpha, num_iter)
        l_data_attack.append(F.nll_loss(
            model(data_attack), label).item())

    plt.plot([int(7./(255*i)) for i in l_alpha], l_data_attack)
    plt.xlabel("Ɛ/α")
    plt.ylabel("Null loss")
    plt.suptitle(
        f"PGD(l_inf) adversial attack with Ɛ={truncate(epsilon,4)}, num_iter={num_iter}")
    return plt.show()

# PGD(alpha,num_iter) : graphique


def visualisation_PGD_alpha_num_iter(model, data, label, epsilon, l_alpha, l_num_iter):
    l_data_attack = []
    for alpha in l_alpha:
        num_max = 0
        max = 0
        for num_iter in l_num_iter:
            data_attack = attacks_PGD.pgd_linf(
                model, data, label, epsilon, alpha, num_iter)
            loss = F.nll_loss(
                model(data_attack), label).item()
            if loss > max:
                max = loss
                num_max = num_iter
        l_data_attack.append([num_max, max])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    a = ax.plot([int(7./(255*i)) for i in l_alpha], [l_data_attack[i][1]
                                                     for i in range(len(l_data_attack))], '-', label='Loss')
    ax2 = ax.twinx()
    b = ax2.plot([int(7./(255*i)) for i in l_alpha], [l_data_attack[i][0]
                                                      for i in range(len(l_data_attack))], '-r', label='Iteration')
    ax.set_xlabel("Ɛ/α")
    ax.set_ylabel("Null loss")
    ax2.set_ylabel("Iteration of max loss")
    lns = a+b
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, loc=0)
    plt.suptitle(
        f"PGD adversial attack")

    return plt.show()

# Macanisme PGD itéraion par itération


def visualisation_PGD(model, data, label, epsilon, num_steps):
    l = []
    x_adv = data.clone().detach().requires_grad_(True)
    prediction = model(x_adv)
    l.append((x_adv, prediction.max(1, keepdim=True)[1]))
    for i in range(num_steps):
        x_adv = attacks_PGD.fgsm_attack(model, x_adv, label, epsilon)
        prediction = model(x_adv)
        l.append((x_adv, prediction.max(1, keepdim=True)[1]))

    for i in range(len(l)):
        plt.subplot(1+len(l)//6, 6, i+1)
        plt.imshow(unnormalize(
            l[i][0].squeeze().detach().cpu().numpy()).transpose((1, 2, 0)))
        plt.title(f"#{i} - {classes[l[i][1]]}")
        plt.axis('off')
    plt.suptitle(f"PGD adversial attack with Ɛ ={epsilon}")
    return plt.show()


if __name__ == "__main__":
    trainloader, testloader, classes = data_CIRFA10.load_CIRFA10(1)

    # Utilisation des données en iter
    dataiter = iter(trainloader)
    data, label = dataiter.next()

    # Chargement du réseau de neurones

    # net = Net()
    # pretrained_model = "./nn_training_path/cifar_net.pth"
    # net.load_state_dict(torch.load(pretrained_model))

    net = nnCIRFA10_91.SimpleDLA()
    pretrained_model = "./Training/param.pth"
    net.load_state_dict(torch.load(
        pretrained_model, map_location=torch.device('cpu')))

    # PGD
    visualisation_PGD_epsilon(net, data, label, [
        float(i)/255 for i in range(0, 20)], 10)

    visualisation_PGD_num_iter(net, data, label, 2/255, 2/(255*3), [
        i for i in range(1, 60)])

    visualisation_PGD_alpha(net, data, label, 2/255,
                            [7./(255*i) for i in range(1, 30)], 25)

    visualisation_PGD_alpha_num_iter(
        net, data, label, 7./255, [7./(255*i) for i in range(10, 29)], [i for i in range(20, 40)])
