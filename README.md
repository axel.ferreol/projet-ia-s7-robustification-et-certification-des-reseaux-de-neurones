## Robustification et Certification des réseaux de neurones

## Résultats

réseau : CIRFA10_91
    
        paramètres:
        dataset = testset
        batch_size = 4
        epsilon = 2/255
        alpha = 2/(255*10)
        num_iter = 15
        num_restart = 3
        p = 0.8
    
    Attack rate
    performance : 0.0936
    fgsm : 0.2429
    pgd_l_inf : 5000 ème traité ! - 33.36
    pgd_l_inf_rand : 1000 ème traité ! - 31.2
    square_attack_l_inf : 0.1957
    square_attack_l2 : 1500 ème traité ! - 15.266666666666667



## Auteurs
Partenariat IRTSystemX et CentraleSupélec
